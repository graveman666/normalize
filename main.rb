if ARGV.size != 16
  raise "wrong number of arguments, they should be 16"
end

(0..15).each do |n|
  unless (0..16).include?(ARGV[n].to_i)
    raise "all arguments should be in range 0 .. 16"
  end
end

numbers = (0..15).map do |n|
  ARGV[n].to_i
end

sum = numbers.reduce(:+)

number = sum / 16.0

p ([number.floor, number.ceil] * 8).join(", ")